module fx
    use, intrinsic :: ISO_C_BINDING
    implicit none

    ! Interface for the callback routine f->c
    interface
        integer(C_INT) function c_call (funct, arg) bind (C)
            use, intrinsic :: ISO_C_BINDING

            type(C_FUNPTR), intent(in), value :: funct
            integer(C_INT), intent(in), value :: arg
        end function c_call
    end interface

    interface
        integer(C_INT) function c_call_ref (funct, arg) bind (C)
            use, intrinsic :: ISO_C_BINDING

            type(C_FUNPTR), intent(in)    :: funct
            integer(C_INT), intent(in)    :: arg
        end function c_call_ref
    end interface

contains

    ! Operation that gets passed to C, C executes.
    ! The operation must be interoperable.

    function square (arg) bind (C)
        integer(C_INT)                      :: square
        integer(C_INT), intent(in), value   :: arg

        ! Name of the function is also the variable name that gets passed as a
        ! return (integer of type C_INT)

        square = arg * arg
        return
    end function square

    function square_ref (arg) bind (C)
        integer(C_INT)                   :: square_ref
        integer(C_INT), intent(in)       :: arg

        square_ref = arg * arg
        return
    end function square_ref

    ! Main C function call
    subroutine execute(val)
        type(C_FUNPTR)                  :: c_fx
        integer(C_INT), intent(in)      :: val

        ! Gets the pointer for the C procedure
        c_fx = C_FUNLOC(square)

        ! Calls the C routine
        write(*,*) "From Fortran: ", c_call(c_fx, val)
    end subroutine execute

    subroutine execute_ref(val)
        type(C_FUNPTR)                  :: c_fx
        integer(C_INT), intent(in)      :: val

        c_fx = C_FUNLOC(square_ref)
        
        ! Calls the C routine
        write(*,*) "From Fortran: ", c_call_ref(c_fx, val)
    end subroutine execute_ref


end module fx
