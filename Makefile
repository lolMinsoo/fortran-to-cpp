CC = gcc
GXX = g++
FC = gfortran

all : f_callback.x

# Compile C First

c_callback.o : c_callback.c
	$(CC) -g -Wall -Wextra -Werror -c c_callback.c

c_callback_ref.o : c_callback_ref.cpp
	$(GXX) -g -Wall -Wextra -Werror -c c_callback_ref.cpp

module_f_callback.o : module_f_callback.f90
	$(FC) -g -O3 -c module_f_callback.f90

f_callback.x : f_callback.f90 c_callback.o module_f_callback.o c_callback_ref.o
	$(FC) -g -o f_callback.x f_callback.f90 c_callback.o module_f_callback.o c_callback_ref.o

clean :
	rm -rf *.o *.x *.mod
