#include <stdio.h>

// Don't need trailing underscore?
int c_call(int (*function)(int), int arg)
{
    char o[100] = "From C, input value: ";
    printf("%s", o);
    printf("%d", arg);
    char p[2] = "\n";
    printf("%s", p);

    // Showcase that arg can be modified
    if (arg == 10)
    {
        arg *= 123;
        return function(arg);
    }
    return function(arg);
}
