program f_callback

use ISO_C_BINDING
use fx

implicit none

integer(C_INT) :: input

! Driver code

input = 2_C_INT
call execute(input)

input = 10_C_INT
call execute(input)

input = 2_C_INT
call execute_ref(input)

input = 10_C_INT
call execute_ref(input)
endprogram
