#include <stdio.h>

#ifdef __cplusplus
extern "C"
#endif
int c_call_ref(int (**function)(int*), int* arg)
{
    char o[100] = "From C, input value: ";
    printf("%s", o);
    printf("%d\n", *arg);
    if (*arg == 10)
        *arg *= 123;
    auto it = (*function)(arg);
    return it;
}
